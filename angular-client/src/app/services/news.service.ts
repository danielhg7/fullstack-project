import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';

@Injectable()
export class NewsService {
    
    env = environment.URL_BACK;

    constructor(public http: HttpClient) {}

    getNews(): Observable<any> {
        const url = `${this.env}/api/news`;
        return this.http.get(url);
    }

    deleteNews(news: any): Observable<any> {
        const url = `${this.env}/api/delete/news`;
        return this.http.put(url, {params: news.objectID });
    }
   
}
