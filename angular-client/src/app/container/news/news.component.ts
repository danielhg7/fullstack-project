import { Component, OnInit, ɵConsole } from '@angular/core';
import { NewsService } from 'src/app/services/news.service';
import { faTrashAlt } from '@fortawesome/free-solid-svg-icons';
@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css']
})
export class NewsComponent implements OnInit {

  newsList: any;
  currentDate: Date = new Date();
  faTrash = faTrashAlt;
  clickedTrash = false;

  constructor(public newsService: NewsService) { }

  ngOnInit() {

    this.newsService.getNews().subscribe(
      result => {
        this.newsList = result;
        this.newsList.news.forEach(news => {
          console.log(typeof news.created_at);
          news.created_at = new Date(news.created_at);
          console.log(typeof news.created_at);
          news.created_at_day = news.created_at.getDate();
          news.created_at_month = news.created_at.getMonth();
          news.created_at_year = news.created_at.getYear();
        });
        console.log(this.newsList.news[0].created_at_day);
        console.log(this.currentDate.getDate());
        console.log(this.newsList);
      }
    );

  }

  openUrl(item: any) {
    if(!this.clickedTrash) {
      if(item.story_url != null){
        window.open(item.story_url, "_blank");
      }
      else if(item.url != null){
        window.open(item.url, "_blank");
      }
    }

    else{
      this.clickedTrash = false;
    }
  }

  deleteNews(item: any) {
    this.clickedTrash = true;
    item.is_deleted = true;
    this.newsService.deleteNews(item).subscribe(
      result => {}
    );
    console.log(item);
  }
}
